import {Component, ElementRef, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {TicketsService} from "../../../services/tickets/tickets.service";
import {ITour, ITourTypeSelect} from "../../../models/tours";
import {TicketsStorageService} from "../../../services/tiсketstorage/tiсketstorage.service";
import {Router} from "@angular/router";
import {BlocksStyleDirective} from "../../../directive/blocks-style.directive";
import {Subscription} from "rxjs";

@Component({
  selector: 'app-ticket-list',
  templateUrl: './ticket-list.component.html',
  styleUrls: ['./ticket-list.component.scss']
})
export class TicketListComponent implements OnInit, OnDestroy {
tickets: ITour[];
loadCountBlock = false;
//свойство для отписки:
tourUnsubscriber: Subscription;
ticketsCopy: ITour[];


  @ViewChild('tourWrap', {read: BlocksStyleDirective}) blockDirective: BlocksStyleDirective;
  @ViewChild('tourWrap') tourWrap: ElementRef;

  constructor( private ticketService: TicketsService,
               private router: Router,
               private ticketStorage: TicketsStorageService) { }

  ngOnInit(): void {
    this.ticketService.getTickets().subscribe(
      (data) => {
        this.tickets = data;
        this.ticketsCopy = [...this.tickets];
        this.ticketStorage.setStorage(data);
      }
    )
//  1 вариант подписки

    this.tourUnsubscriber = this.ticketService.ticketType$.subscribe((data: ITourTypeSelect) => {
      console.log('data', data)

      let ticketType: string;
      switch (data.value) {
        case "single":
          this.tickets = this.ticketsCopy.filter((el) => el.type === "single");
          break;
        case "multi":
          this.tickets = this.ticketsCopy.filter((el) => el.type === "multi");
          break;
        case "all":
          this.tickets = [...this.ticketsCopy];
          break;

      }
      setTimeout(() => {

        this.blockDirective.updateItems();

        this.blockDirective.initStyle(0);  // сбрасываем индекс на 0 элемент
      });

    });

//  2 вариант подписки
//     this.tourUnsubscriber = this.ticketService.getTicketTypeObservable().subscribe((data:ITourTypeSelect) => {  console.log('data', data)  });

  }

  goToTicketInfoPage(item: ITour) {
    // this.router.navigate( ['/tickets/ticket/${item.id}'])
    // если пусть в роутинг модуле записан так: path: 'tickets/:id',

    this.router.navigate( ['/tickets/ticket'], {queryParams:{id:item.id}})
    // если пусть в роутинг модуле записан так: path: 'ticket',
  }

  directiveRenderComplete (ev:boolean) {
    const el: HTMLElement= this.tourWrap.nativeElement;
    el.setAttribute('style', 'background-color: #f8f9fa')
    this.blockDirective.initStyle(3);
    this.loadCountBlock = true ;
  }


 ngOnDestroy() {
  this.tourUnsubscriber.unsubscribe();
}


}
