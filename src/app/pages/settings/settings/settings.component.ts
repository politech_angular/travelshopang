import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subject, Subscription} from "rxjs";
import {ObservableExampleService} from "../../../services/testing/observable-example.service";

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss']
})
export class SettingsComponent implements OnInit, OnDestroy {
  private subjectScope: Subject<string>;
  private subjectUnsubscribe: Subscription;

  constructor( private testing: ObservableExampleService) { }

  ngOnInit(): void {
    // присвоить в него результат вызова метода  ObservableExampleService getSubject
    this.subjectScope=this.testing.getSubject() ;

    //подписаться на данные от subjectScope и вывести в консоли полученные данные
    this.subjectUnsubscribe = this.subjectScope.subscribe((data)=>{
      console.log('data',data)
    });

    // отправить данные с помощью subjectScope.next
    this.subjectScope.next('данные')

  }

  ngOnDestroy():void{
    this.subjectUnsubscribe .unsubscribe()
  }
}
