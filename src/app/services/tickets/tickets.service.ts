import { Injectable } from '@angular/core';
import {Observable, Subject} from "rxjs";
import {ITour, ITourTypeSelect} from "../../models/tours";
import {TicketsRestService} from "../rest/tickets-rest.service";

@Injectable({
  providedIn: 'root'
})
export class TicketsService {

  private ticketSubject = new Subject<ITourTypeSelect>();

  constructor(private ticketsServiceRest:  TicketsRestService) { }


 getTickets(): Observable<ITour[]>{
  return this.ticketsServiceRest.getTickets();
}

// 1 вариант доступа к Observable
  readonly ticketType$ = this.ticketSubject.asObservable();

// 2 вариант доступа к Observable
//
//   getTicketTypeObservable(): Observable<ITourTypeSelect> {
//     return this.ticketSubject.asObservable();
//   }

  updateTour(type:ITourTypeSelect): void {
    this.ticketSubject.next(type);
  }

}
